'''
Created on 19/09/2020

@author: rafael
'''
from ntpath import split
from lib2to3.pgen2.token import OP
class TDAString:
    '''Clase para trabajar TDA´S en string'''
    def __init__(self,cadena):
        self.cadena=""
        self.cadena=cadena
    def getcadena(self):
        return self.cadena
    def setcadena(self,cadena):
        self.cadena=cadena
        
    def mostrarCadenaInvertidaporLetraPalabras(self):
        #Invertida
        print("Cadena invertida")
        i=len(self.cadena)-1
        for i in range(i,-1,-1):
            print(self.cadena[i], end=",")
        print()
        print("Cadena por palabras")
        listaPalabras=self.cadena.split(" ")
        i=0
        for i in range(i,len(listaPalabras)):
            print(listaPalabras[i])

    def agregarEliminarCaracteresYoSubcadenasPosicionesEspecificas(self,opcionUsuario):
        if(opcionUsuario=="A"):
            try:
                poci=int(input(f"En que pocicion deseas agregar tu caracter (Que sea menor a {len(self.cadena)}): "))
                carac=input("Ingresa el caracter por favor: ")
                nuevaCad=""
                i=0
                poci=poci-1
                if (poci<len(self.cadena)):
                    for i in range(i,len(self.cadena)):
                        if(i==poci):
                            nuevaCad=nuevaCad+carac
                        else:
                            nuevaCad=nuevaCad+self.cadena[i]
                    self.cadena=nuevaCad
                else:
                    print("La pocicion que ingresaste es no existe :(")
                    
            except ValueError:
                print("NO ingresaste un valor numerico entero")
                print("Resultado: ",self.cadena)
        elif(opcionUsuario=="B"):
            nuevaCad=""
            try:
                poci=int(input("Que pocicion deseas Eliminar "))
                i=0
                for i in range(i,len(self.cadena)):
                        if(i!=poci-1):
                            nuevaCad=nuevaCad+self.cadena[i]
                self.cadena=nuevaCad
            except ValueError:
                print("NO ingresaste un valor numerico")
            print("Resultado: ",self.cadena)
        elif(opcionUsuario=="C"):
            subcad=input("Ingresa la sub cadena que deseas eliminar Ejemplo(5-2): ")
            indices=[]
            indices=subcad.split("-")
            num1=int(indices[0])
            num2=int(indices[1])
            nuevaCad=""
            i=0
            if(num1<len(self.cadena) or num2<len(self.cadena)):
                for i in range(i,len(self.cadena)):
                    if(not(i>num1 and i<num2)):
                        nuevaCad=nuevaCad+self.cadena[i:i+1]
            else:
                print("Los limites que ingresaste no existen")
            self.cadena=nuevaCad
            print("Resultado: ",self.cadena)
        elif(opcionUsuario=="D"):
            poci=0
            bandera=False
            subcad=input("Ingresa la cadena que deseas aregar")
            try:
                poci=int(input("Desde que pocicion deseas agregarla"))
            except ValueError:
                print("Por favor ingresa un valor numerico entero")
                bandera=True
            if(bandera==False):
                nuevaCad=""
                i=0
                for i in range(i,len(self.cadena)):
                    if(poci==i-1):
                        nuevaCad=nuevaCad+subcad
                    else:
                        nuevaCad=nuevaCad+self.cadena[i]
                self.cadena=nuevaCad
                print(f"Resultado: {self.cadena}")
            else:
                print("Vaya al parecer hiciste un llenado incorrecto")
    def mostrarCadenaMayusculasCadaPalabra(self):
        i=0
        numEspacios=0
        nuevaCad=""
        for i in range(i,len(self.cadena)):
            if(self.cadena[i]==" "):
                numEspacios=numEspacios+1
        if(numEspacios>0):
            poci=""
            listaPalabras=self.cadena.split(" ")
            i=0
            x=0
            nuevaCad=""
            for i in range(i,len(listaPalabras)):
                poci=listaPalabras[i]
                x=0
                for x in range(x,len(poci)):
                    if(x==0):
                        nuevaCad=nuevaCad+poci[x].upper()
                    else:
                        nuevaCad=nuevaCad+poci[x]
                nuevaCad=nuevaCad+" "
            self.cadena=nuevaCad
        else:
            self.cadena=self.cadena.title()
            print("Resultado: ",self.cadena)
    def mostrarCadenaCaMeLCaSe(self):
        i=0
        for i in range(i,len(self.cadena)):
            poci=self.cadena[i]
            if(i%2==0):
                print(poci.lower(),end ="")
            else:
                print(poci.upper(),end="")
           
fraseDefault=input("Ingresa una frase o palabra para poder prosegir: ")

catda=TDAString(fraseDefault)
opcion=0
bandera=False
while (bandera==False):
    opcion=0
    print("Elige una opcion:")
    print("1-Mostrar cadena invertida y mostrarla por palabras")
    print("2-Agregar o eliminar Caracteres / Subcadenas en Pociciones Especificas")
    print("3-Mostar la cadena con la primer letra de cada palabra en mayúscula")
    print("4-Mostrar Cadena en formato CaMeL CaSe especial")
    print("5-Salir")
    try:
        opcion=int(input())
    except ValueError:
        print("NO ingresaste un valor numerico")
        
    if(opcion==1):
        catda.mostrarCadenaInvertidaporLetraPalabras()
    elif(opcion==2):
        print("Elige una opcion")
        print("A) Agregar caracter en pocicion especifica")
        print("B) Eliminar caracter especifico en 'X' pocicion")
        print("C) Borrar subcadena")
        print("D) Agregar sub cadena")
        menupun2=input().upper()
        if(menupun2=="A"):
            catda.agregarEliminarCaracteresYoSubcadenasPosicionesEspecificas(menupun2)
            print(catda.getcadena())
        elif(menupun2=="B"):
            catda.agregarEliminarCaracteresYoSubcadenasPosicionesEspecificas(menupun2)
        elif(menupun2=="C"):
            catda.agregarEliminarCaracteresYoSubcadenasPosicionesEspecificas(menupun2)
        elif(menupun2=="D"):
            catda.agregarEliminarCaracteresYoSubcadenasPosicionesEspecificas(menupun2)
        else:
            print("Error no ingresaste una opcion disponible")
    elif(opcion==3):
        catda.mostrarCadenaMayusculasCadaPalabra()
        print(catda.getcadena())
    elif(opcion==4):
        catda.mostrarCadenaCaMeLCaSe()
        bandera=True
    elif(opcion==5):
        print("Saliendo...")
    else:
        if(not opcion==0):
            print("La opcion que ingresaste no existe")
    
        






